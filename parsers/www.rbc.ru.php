<?php declare(strict_types=1);

use GuzzleHttp\Client;
use Entities\News;
use Opis\ORM\EntityManager;
use Entities\Selectors\Rbc\NewsBody;
use Entities\Selectors\Rbc\NewsList;
use TextCutter\TextCutter;

/**
 * @var EntityManager $orm
 * @var Client $client
 */

// Получение заголовков новостей и ссылок с главной страницы
$response = $client->get('https://www.rbc.ru/');
$newsList = new NewsList($response->getBody()->getContents());
$newsTitles = $newsList->getNewsTitles();
$modifiedTimestamp = $newsList->getLastNewsTimestamp();

// Получение оставшихся заголовков новостей и ссылок через api
$response = $client->get("https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews/lastDate/$modifiedTimestamp/limit/22");
foreach (json_decode($response->getBody()->getContents(), true)['items'] as $item) {
    $newsList = new NewsList($item['html']);
    $newsTitles = array_merge($newsTitles, $newsList->getNewsTitles());
}


$count = 1;
foreach ($newsTitles as $link => $title) {
    if ($count > 15) continue;

    $response = $client->get($link);
    $newsBody = new NewsBody($response->getBody()->getContents());

    $textCutter = new TextCutter(TextCutter::METHOD_WHOLE_WORDS, TextCutter::DECORATION_ELLIPSIS);

    $news = $orm->query(News::class)
        ->where('link')->is($link)
        ->get();
    if (is_null($news)) {
        $news = $orm->create(News::class);
    }
    $news->setTitle($title)
        ->setImage($newsBody->getNewsMainImage())
        ->setBody($content = $newsBody->getNewsContent())
        ->setPreview($textCutter->cut($content))
        ->setLink($link);
    if (!$orm->save($news)) {
        throw new Exception("Error saving news {$title} ($link)");
    }

    $count++;
}