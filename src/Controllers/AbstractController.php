<?php declare(strict_types=1);


namespace Controllers;


use App\DB;
use Opis\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class AbstractController
{
    public EntityManager $orm;
    public Environment $twig;

    public function __construct()
    {
        $this->orm = DB::getInstance();

        $loader = new FilesystemLoader('../templates');
        $this->twig = new Environment($loader, [
            'cache' => '../var/cache/compilation_cache',
            'debug' => true
        ]);
    }

    /**
     * @param $name
     * @param array $context
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render($name, array $context = []): Response
    {
        return new Response($this->twig->render($name, $context));
    }
}