<?php declare(strict_types=1);


namespace Controllers;


use Entities\News;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends AbstractController
{
    public function index(): Response
    {
        $news = $this->orm->query(News::class)->all(['id', 'title', 'preview']);

        return $this->render('index.html.twig', ['news' => $news]);
    }

    public function show($id): Response
    {
        $news = $this->orm->query(News::class)
            ->where('id')->is($id)
            ->get(['title', 'body', 'image']);

        return $this->render('show.html.twig', ['news' => $news]);
    }
}