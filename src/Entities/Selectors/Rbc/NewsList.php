<?php declare(strict_types=1);


namespace Entities\Selectors\Rbc;


use DiDom\Query;
use Entities\Selectors\SelectorsAbstract;

class NewsList extends SelectorsAbstract
{
    public function getNewsTitles(): array
    {
        $titles = [];
        foreach ($this->find('//a[@class="news-feed__item js-news-feed-item js-yandex-counter"]') as $element) {
            // Убираем рекламные ссылки
            if (
                false !== stripos($element->getAttribute('href'), 'traffic.rbc.ru')
                or false !== stripos($element->getAttribute('href'), 'pro.rbc.ru')
            ) {
                continue;
            }

            $title = $element->first('//span[1]', Query::TYPE_XPATH);
            $titles[$element->getAttribute('href')] = trim($title->text());
        }
        return $titles;
    }

    public function getLastNewsTimestamp(): int
    {
        foreach ($this->find('//a[@class="news-feed__item js-news-feed-item js-yandex-counter"]') as $element) {
            $timestamp = (int)$element->getAttribute('data-modif');
        }

        return $timestamp;
    }
}