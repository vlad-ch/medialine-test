<?php declare(strict_types=1);


namespace Entities\Selectors\Rbc;


use Entities\Selectors\SelectorsAbstract;

class NewsBody extends SelectorsAbstract
{
    public function getNewsMainImage(): ?string
    {
        if (!is_null($element = $this->first('//img[@class="g-image article__main-image__image"]'))) {
            return $element->getAttribute('src');
        }
        return null;
    }

    public function getNewsContent(): string
    {
        $paragraphs = '';
        $xpaths = [
            '//div[@class="article__text article__text_free"]/p',
            '//div[@class="article__text article__text_free"]/div[@class="article__special_container"]/p',
            '//div[@class="article__text"]/p',
        ];
        foreach ($this->find(implode(' | ', $xpaths)) as $element) {
            $paragraphs .= "\n" . trim($element->text());
        }

        if ($paragraphs === '') {
            foreach ($this->find('//div[@class="article__text article__text_free"]') as $element) {
                $paragraphs .= "\n" . trim($element->text());
            }
        }

        return $paragraphs;
    }
}