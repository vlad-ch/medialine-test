<?php declare(strict_types=1);


namespace Entities\Selectors;


use DiDom\Document;
use DiDom\Element;
use DiDom\Exceptions\InvalidSelectorException;
use DiDom\Query;
use DOMElement;
use Exception;
use InvalidArgumentException;

abstract class SelectorsAbstract
{
    protected string $dom;
    protected Document $document;

    /**
     * @throws Exception
     */
    public function __construct(string $dom, $encoding = 'UTF-8')
    {
        $this->dom = iconv($encoding, 'UTF-8//IGNORE', $dom);
        $this->dom = html_entity_decode($this->dom, ENT_QUOTES | ENT_XHTML, 'UTF-8');
        $this->dom = (string)preg_replace('~\s+~', ' ', trim($this->dom));
        $this->document = new Document($this->dom, false,'UTF-8');
    }


    /**
     * Searches for a node in the DOM tree and returns first element or null.
     *
     * @param string $expression XPath expression or a CSS selector
     * @param string $type The type of the expression
     * @param bool $wrapNode Returns array of Element if true, otherwise array of DOMElement
     * @param DOMElement|null $contextNode The node in which the search will be performed
     *
     * @return Element|DOMElement|string|null
     *
     * @throws InvalidSelectorException if the selector is invalid
     */
    protected function first(string $expression, string $type = Query::TYPE_XPATH, bool $wrapNode = true, DOMElement $contextNode = null)
    {
        return $this->document->first($expression, $type, $wrapNode, $contextNode);
    }


    /**
     * Searches for a node in the DOM tree for a given XPath expression or CSS selector.
     *
     * @param string $expression XPath expression or a CSS selector
     * @param string $type The type of the expression
     * @param bool $wrapNode Returns array of Element if true, otherwise array of DOMElement
     * @param DOMElement|null $contextNode The node in which the search will be performed
     *
     * @return Element[]|DOMElement[]
     *
     * @throws InvalidSelectorException if the selector is invalid
     * @throws InvalidArgumentException if context node is not DOMElement
     */
    protected function find(string $expression, string $type = Query::TYPE_XPATH, bool $wrapNode = true, DOMElement $contextNode = null): array
    {
        return $this->document->find($expression, $type, $wrapNode, $contextNode);
    }
}
