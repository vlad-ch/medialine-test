<?php declare(strict_types=1);


namespace Entities;


use Opis\ORM\Entity;
use Opis\ORM\IEntityMapper;
use Opis\ORM\IMappableEntity;

class News extends Entity implements IMappableEntity
{
    /**
     * @inheritDoc
     */
    public static function mapEntity(IEntityMapper $mapper)
    {
        $mapper->table('news');
        $mapper->cast([
            'id' => 'int',
            'title' => 'string',
            'preview' => 'string',
            'body' => 'string',
            'link' => 'string',
            'created_at' => 'date',
            'updated_at' => '?date'
        ]);
        $mapper->useTimestamp();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->orm()->getColumn('id');
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->orm()->getColumn('title');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setTitle(string $value): self
    {
        $this->orm()->setColumn('title', $value);
        return $this;
    }


    /**
     * @return string
     */
    public function getPreview(): string
    {
        return $this->orm()->getColumn('preview');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setPreview(string $value): self
    {
        $this->orm()->setColumn('preview', $value);
        return $this;
    }


    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->orm()->getColumn('body');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setBody(string $value): self
    {
        $this->orm()->setColumn('body', $value);
        return $this;
    }


    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->orm()->getColumn('image');
    }

    /**
     * @param string|null $value
     * @return $this
     */
    public function setImage(?string $value): self
    {
        $this->orm()->setColumn('image', $value);
        return $this;
    }


    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->orm()->getColumn('link');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setLink(string $value): self
    {
        $this->orm()->setColumn('link', $value);
        return $this;
    }
}