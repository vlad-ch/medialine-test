<?php declare(strict_types=1);


namespace App;


use Opis\Database\Connection;
use Opis\ORM\EntityManager;

class DB
{
    private static EntityManager $instance;

    public static function getInstance(): EntityManager
    {
        if (empty(self::$instance)) {
            try {
                $connection = new Connection('sqlite:../data/database.sqlite');
                $connection->initCommand('PRAGMA encoding = "UTF-8";');
                $connection->initCommand('PRAGMA journal_mode = WAL;');
                $connection->initCommand('PRAGMA page_size = 4086;'); // blockdev --getbsz /dev/sda
                $connection->initCommand('PRAGMA synchronous = NORMAL;');
                $connection->logQueries();
                self::$instance = new EntityManager($connection);
            } catch (\Exception $error) {
                echo $error->getMessage();
            }
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    /**
     * @throws \Exception
     */
    final public function __clone()
    {
        throw new \Exception('Feature disabled.');
    }

    /**
     * @throws \Exception
     */
    final public function __wakeup()
    {
        throw new \Exception('Feature disabled.');
    }
}