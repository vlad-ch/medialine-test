<?php declare(strict_types=1);

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add('news_index', new Routing\Route('/', [
    'id' => null,
    '_controller' => 'Controllers\NewsController::index',
]));
$routes->add('news_show', new Routing\Route('/news/{id}', [
    'id' => null,
    '_controller' => 'Controllers\NewsController::show',
]));

return $routes;