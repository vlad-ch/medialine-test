#!/usr/bin/env php
<?php declare(strict_types=1);

use App\DB;
use GuzzleHttp\Client;
use League\CLImate\CLImate;


if (substr(PHP_SAPI, 0, 3) != 'cli') {
    exit("Only CLI mode.");
}


function includeIfExists(string $file): bool
{
    return file_exists($file) && include $file;
}

if (!includeIfExists('../vendor/autoload.php')) {
    fwrite(STDERR, 'Install dependencies using Composer.' . PHP_EOL);
    exit(1);
}


$climate = new CLImate;
$climate->arguments->add([
    'run' => [
        'prefix' => 'r',
        'longPrefix' => 'run',
        'required' => true,
    ]
]);

try {
    $climate->arguments->parse();
} catch (Exception $e) {
    $climate->to('error')->red($e->getMessage());
    $climate->br();
    $climate->usage();
    die();
}


$orm = DB::getInstance();


if (!empty($climate->arguments->get('run'))) {
    try {
        $client = new Client([
            'allow_redirects' => [
                'max' => 5,
                'strict' => false,
                'referer' => false,
                'protocols' => ['http', 'https'],
                'track_redirects' => true
            ],
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
            ]
        ]);

        require_once(sprintf(__DIR__ . '/../parsers/%s.php', $climate->arguments->get('run')));
    } catch (Throwable $e) {
        fwrite(STDERR, 'Error! ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString() . PHP_EOL);
        exit(1);
    }
}